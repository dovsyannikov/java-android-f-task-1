package com.company;

public class Generator {
    public static Smartphone [] generate() {
        Smartphone [] smartphones = new Smartphone[5];
        {
            smartphones[0] = new Smartphone("LG",4.7f,322.4f);
            smartphones[1] = new Smartphone("LG",4.8f,122.4f);
            smartphones[2] = new Smartphone("Samsung",4.0f,341.4f);
            smartphones[3] = new Smartphone("Iphone",5.7f,922.4f);
            smartphones[4] = new Smartphone("Lenovo",5.5f,422.4f);
        }
        return smartphones;
    }
}
